// @ts-nocheck
const {
  contextBridge,
  ipcRenderer
} = require("electron");
const remote = require('@electron/remote');
const { dialog, Menu } = remote;
const fs = require('fs');
const jQuery = require('jquery');
const bootstrap = require('bootstrap');
const lisense = require('./license.json');

require ( './bootstrap.min.css');
require ( './bootstrap.bundle.min.js');
require ( './style.css');

/******************************* *
 * Save
/******************************* */

var windowTopBar = document.createElement('div')
windowTopBar.style.width = "100%"
windowTopBar.style.height = "86px"
windowTopBar.style.backgroundColor = "transparent"
windowTopBar.style.position = "absolute"
windowTopBar.style.top = windowTopBar.style.left = 0
windowTopBar.style.top = windowTopBar.style.zIndex = 999999
windowTopBar.style.webkitAppRegion = "drag"
document.body.appendChild(windowTopBar);


jQuery(function($) {

  function showNoti(  textx = '' ) {
    const toastLiveExample = document.getElementById('liveToast');
    $(toastLiveExample).find('.toast-body').html(textx);
    const toast = new bootstrap.Toast(toastLiveExample);
    toast.show();
  }

  ipcRenderer.send('initial_data', true);
  ipcRenderer.on('initial_data', (e, data) => {
    if ( typeof data.campaign_link !== 'undefined') {
      document.getElementById('campaign_link').value = data.campaign_link;
    }
    if ( typeof data.campaign_license_key !== 'undefined') {
      document.getElementById('campaign_license_key').value = data.campaign_license_key;
    }
  });

  /*******
   * Nhận notification từ main.js
   */
  ipcRenderer.on('notification', (e, data) => {
    showNoti(data);
  });


  let saveBtn = document.getElementById('saveBtn');
  if ( saveBtn ) {
    saveBtn.onclick =  async() => {
      // let campaign_form = document.getElementById("campaign_form");
      let campaign_link = document.getElementById('campaign_link').value;
      let campaign_license_key = document.getElementById('campaign_license_key').value;

      // console.log(lisense, 'lisense'); 
      // license-key
      let keyvalid = false;
      for ( let k of lisense ) {
        if ( k['license-key'] === campaign_license_key) {
          let validDate = k['license-valid-until'];
          let convertDateToTime = new Date(validDate).getTime();
          let now = new Date().getTime();
          if ( convertDateToTime > now ) {
            keyvalid = true;
          }
        }
      }

      if ( ! keyvalid ) {
        showNoti('Key đã hết hạn, bạn kiểm tra lại!');
        return;
      }
      ipcRenderer.send('campaign', {
        campaign_link,
        campaign_license_key,
      });
      showNoti('Đã lưu thông tin vào bộ nhớ!');
    }
  }


});




/**
 * Save // end
 */

const startBtn = document.getElementById('startBtn');
startBtn.onclick = e => {
  ipcRenderer.send('start_recording', true);
  startBtn.classList.add('is-danger');
  startBtn.innerText = 'Recording';
};

const stopBtn = document.getElementById('stopBtn');
stopBtn.onclick = e => {
  ipcRenderer.send('stop_recording', {});
  startBtn.classList.remove('is-danger');
  startBtn.innerText = 'Start';
};

const closeBtn = document.getElementById('closeBtn');
closeBtn.onclick = e => {
  ipcRenderer.send('close_window', {});
};



ipcRenderer.send('main', 'buffer');
