// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;
declare const SUB_WINDOW_WEBPACK_ENTRY: string;
declare const SUB_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

import { app, BrowserWindow, globalShortcut, ipcMain, protocol, Menu } from 'electron';
import path from 'path';
// https://github.com/electron/rcedit
const root_path = path.join(app.getAppPath(), "/src");
import fs from 'fs';
// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}
import lisense from './license.json';
// var cmd = process.argv[1];
// if (cmd == '--squirrel-firstrun') {
//   // Running for the first time.
// }


/**!SECTION
 * NOTE
 */
console.log('CMD or Ctr + W for Exit');
console.log('CMD or Ctr + SHIFT + C for test CHAT');
console.log('CMD or Ctr + SHIFT + L for test LIKE');
console.log('CMD or Ctr + SHIFT + S for test SHARE');
console.log('CMD or Ctr + SHIFT + G for test GIFT');

import testing from './testing';
const testingClass = new  testing();
import { WebcastPushConnection } from 'tiktok-live-connector';
import storage from 'electron-json-storage';
storage.setDataPath(app.getPath('userData'));


let currentData: any  = storage.getSync('campaign');
let mainWindow: BrowserWindow = undefined;
const childScreentoshow: BrowserWindow = undefined;
let tiktokLiveConnection: WebcastPushConnection = undefined;

// require('@electron/remote/main').initialize();

import * as remoteMain from '@electron/remote/main';
remoteMain.initialize();

const Thispartition = 'persist:liveStreaming';
const isWin = process.platform === "win32";


(() => {
  if ( typeof  currentData.campaign_license_key === 'undefined') return;
  for ( const k of lisense ) {
    if ( k['license-key'] === currentData.campaign_license_key) {
      const validDate = k['license-valid-until'];
      const convertDateToTime = new Date(validDate).getTime();
      const now = new Date().getTime();
      if ( convertDateToTime < now ) {
        storage.clear(function(error) {
          if (error) throw (error);
        });
      }
    }
  }
})();



/************************************************************************************************ *
 * Big changed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/************************************************************************************************ */

/**
 * List directory ...
 * @param {*} rootPath 
 * @returns 
 */

protocol.registerSchemesAsPrivileged([
  { scheme: 'http', privileges: { standard: true, bypassCSP: true, allowServiceWorkers: true, supportFetchAPI: true, corsEnabled: true, stream: true } },
  { scheme: 'https', privileges: { standard: true, bypassCSP: true, allowServiceWorkers: true, supportFetchAPI: true, corsEnabled: true, stream: true } },
  { scheme: 'mailto', privileges: { standard: true } },
  { scheme: 'file', privileges: { standard: true } },
]);

/**
 * Extract username from a link
 * @param {*} link 
 * @returns 
 */
function extractUsername( link: string ) {
  try {
    return String(link).split('@')[1].split('/')[0];
  } catch (e) {
    return  '';
  }
}


const subWindow =  (mainWindow) => {
  let screenplay = root_path + '/screenplay/index.html';
  try {
    fs.accessSync(screenplay, fs.constants.R_OK);
    mainWindow.loadURL(SUB_WINDOW_WEBPACK_ENTRY);
  } catch(e) {
    console.log('Screenplay not found... %s', e);
    screenplay = 'src/404.html';
    mainWindow.loadURL(screenplay);
  }


    // childScreentoshow.webContents.setAudioMuted({mute: true});
    // childScreentoshow.webContents.openDevTools({mode: 'detach'});  

    let checkIntervalData: string | number | NodeJS.Timer;
  // subnotification
  ipcMain.on('subwindow_connect', async(event, packet) => {
    testingClass.set(event);
    /**
     * 
     * 
    member
    chat
    gift
    roomUser
    like
    social
    emote
    envelope
    questionNew
    linkMicBattle
    linkMicArmies
    liveIntro
    subscribe
     */
    checkIntervalData = setInterval( () => {
      const currentData  = storage.getSync('campaign');
      // Username of someone who is currently live
      const tiktokUsername = extractUsername(currentData.campaign_link);
      if ( ! tiktokUsername ) {
        console.log('tiktokUsername not found!')
        return;
      }
      console.log(tiktokUsername, '::: fetching ...')
      // Create a new wrapper object and pass the username
      tiktokLiveConnection = new WebcastPushConnection(tiktokUsername, {
        processInitialData: false,
        enableExtendedGiftInfo: true,
        enableWebsocketUpgrade: true,
        requestPollingIntervalMs: 2000
      });
      // https://github.com/zerodytrash/TikTok-Live-Connector
      // Connect to the chat (await can be used as well)
      tiktokLiveConnection.connect().then(state => {
        console.info(`Connected to roomId ${state.roomId}`);
        clearInterval(checkIntervalData);
      }).catch(err => {
          console.error('Failed to connect, retry...');
          // console.log(err);
      })
  
      // Define the events that you want to handle
      // In this case we listen to chat messages (comments)
      tiktokLiveConnection.on('chat', data => {
          console.log(`${data.uniqueId} (userId:${data.userId}) writes: ${data.comment}`);
          event.reply('chat', data);
          // event.reply( 'comment', `<span class="text-name">${data.nickname}</span>: <span class="text-message">${data.comment}</span>` );
      })
  
      // And here we receive gifts sent to the streamer
      tiktokLiveConnection.on('gift', data => {
        // console.log(data, '<<<---- gift ------');

          event.reply('gift', data);
          // event.reply( 'share', {profilePictureUrl: data.profilePictureUrl, nickname: data.nickname});
      });
  
      // And like stream
      tiktokLiveConnection.on('like', data => {
        event.reply('like', data);
        console.log(`${data.uniqueId} sent ${data.likeCount} likes, total likes: ${data.totalLikeCount}`);
      })
  
      // And share stream
      tiktokLiveConnection.on('social', data => {
          event.reply('social', data);
          console.log(`${data.uniqueId} (userId:${data.userId}) social!`);
      });

     // Triggered every time someone sends a treasure chest.
      tiktokLiveConnection.on('envelope', data => {
          event.reply('envelope', data);
          // console.log('envelope received', data);
      })

      // Triggers when a user follows the streamer. Based on social event.

      tiktokLiveConnection.on('follow', (data) => {

        event.reply('follow', data);
          console.log(data.uniqueId, "followed!");
      });

      // Triggers when a user shares the stream. Based on social event.
      // let shareTotal = 0;
      tiktokLiveConnection.on('share', (data) => {
        event.reply('share', data);
        // shareTotal++;
        // combat();
        // event.reply( 'share', {profilePictureUrl: data.profilePictureUrl, nickname: data.nickname});
        console.log(data.nickname, "shared the stream!");
      });

    }, 6000);

  });

  return mainWindow;

}


const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 546, 
    height: 960, 
    frame: false,
    icon: root_path + '/screenplay/images/icon.ico',
    // titleBarStyle: 'hidden',
    resizable: false,
    webPreferences: {
      preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
      defaultEncoding: 'UTF-8',
      spellcheck: false,
      enableWebSQL: true,
      partition: Thispartition,
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true
    }
  });
  
  remoteMain.enable(mainWindow.webContents);
  // and load the index.html of the app.
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
  // mainWindow.webContents.on('did-finish-load', function() {});

  // Open the DevTools.   <-------------------------------------
  // mainWindow.webContents.setAudioMuted(true);  
  // mainWindow.webContents.openDevTools({mode: 'detach'});  


  mainWindow.on('did-start-navigation', function () {
      session.fromPartition(Thispartition).cookies.flushStore();
  });

  mainWindow.on('did-navigate', function () {
      session.fromPartition(Thispartition).cookies.flushStore();
  });
  
    /**
     * Add an IPC event listener for the channel
     */ 

    // ipcMain.on('stop_recording', async(event, packet) => {
    //   console.log('recording stop');
    //   try {
    //     childScreentoshow.close();
    //     // mainWindow.close();
    //   } catch (e) { /* empty */ }
    //   event.reply('notification', 'Đã dừng mọi tiến trình livestream!');
    // });

    ipcMain.on('start_recording', async(event, packet) => {
        // to the renderer that sent the original message
      if ( typeof currentData.campaign_license_key === 'undefined' || currentData.campaign_license_key === ''  ) {
        event.reply('notification', 'Chưa có giấy phép để bắt đầu!');
      } else {
        event.reply('notification', 'Bắt đầu nào!');
        subWindow(mainWindow);
      }
    });

    ipcMain.on('close_window', async() => {
      mainWindow.close();
      process.exit();
    });

    ipcMain.on('initial_data', async(event, packet) => {
      const data = storage.getSync('campaign');
      event.reply('initial_data', data);
    });

    ipcMain.on('campaign', async(event, packet) => {
      /** Show the request data */
      // const fileStream = fs.createWriteStream( root_path +'/cache/'+ 'finalvideo.webm', {flags: 'a'});
      // fileStream.write(packet);
      storage.set('campaign', packet, function(error) {
        if (error) throw error;
      });
      // console.log(packet);
      // await fs.promises.writeFile( root_path +'/cache/'+ Math.random() + '.webm', packet, 'binary');
      currentData = packet;
    })

  return mainWindow;
};



// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.commandLine.appendSwitch('ignore-certificate-errors');

function closeAllWindows() {
  try {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
    ipcMain.removeAllListeners();
    childScreentoshow.close();
    mainWindow.close();
  } catch (e) { /* empty */ }
  process.exit();
}

// set menu to null help start faster...
Menu.setApplicationMenu(null) 
app.on('ready', () => {
  globalShortcut.register("CommandOrControl+W", () => {
    //stuff here
    closeAllWindows();
  });
  /**!SECTION
   * Test chat
   */
  globalShortcut.register("CommandOrControl+Shift+C", () => {
    testingClass.chat();
  });
   /**!SECTION
    * LIKE
    */
  globalShortcut.register("CommandOrControl+Shift+L", () => {
    testingClass.like();
  });
  /**!SECTION
   * SHARE
   */
  globalShortcut.register("CommandOrControl+Shift+S", () => {
    testingClass.share();
  });
  /**!SECTION
   * follow
   */
  globalShortcut.register("CommandOrControl+Shift+F", () => {
    testingClass.follow();
  });
  /**!SECTION
   * gift
   */
  globalShortcut.register("CommandOrControl+Shift+G", () => {
    testingClass.gift();
  });


  createWindow();
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});


app.on('window-all-closed', closeAllWindows);

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.


const createMenu = () => {
  const application = {
    label: 'Application',
    submenu: [
      {
        label: 'About ICEO',
        role: 'about',
        click: () => {
          app.quit();
        },
      },
      {
        type: 'separator',
      },
      {
        label: 'Quit',
        accelerator: 'Command+Q',
        click: () => {
          app.quit();
        },
      },
    ],
  };

  const edit = {
    label: 'Edit',
    submenu: [
      {
        label: 'Undo',
        accelerator: 'CmdOrCtrl+Z',
        role: 'undo',
      },
      {
        label: 'Redo',
        accelerator: 'Shift+CmdOrCtrl+Z',
        role: 'redo',
      },
      {
        type: 'separator',
      },
      {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        role: 'cut',
      },
      {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        role: 'copy',
      },
      {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        role: 'paste',
      },
      {
        label: 'Select All',
        accelerator: 'CmdOrCtrl+A',
        role: 'selectAll',
      },
    ],
  };

  const template = [application, edit];

  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
};

createMenu();