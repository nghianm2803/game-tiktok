// @copyright jamviet.com
// fromm ICEO

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
function random(_array) {
  if (!Array.isArray(_array)) return;
  return _array[Math.floor(Math.random() * _array.length)];
}

export default class TestInteract {
  event: any | null | undefined;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  set(event: any) {
    this.event = event;
  }

  chat() {
    if (!this.event) return;
    const commentTexts = ["123", "456"];
    const commentText = random(commentTexts);
    const testingData = {
      comment: String(commentText),
      userId: 6639506414 + Math.random() * 10,
      secUid:
        "MS4wLjABAAAAI0hrPNJuilzL_xzgrHfB19NJX2n5VDG-4r_8TYzt0XPplJf-ClYIYQaax-W81ClW",
      uniqueId: "31493796937",
      nickname: "Vũ Văn Cương270",
      profilePictureUrl:
        "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
      followRole: 0,
      userDetails: {
        createTime: "0",
        bioDescription: "",
        profilePictureUrls: [
          "https://p16-sign-sg.tiktokcdn.com/tiktok-obj/1625007705715714~tplv-tiktok-shrink:72:72.webp?x-expires=1671868800&x-signature=z6%2B17D0XaZXiSmdU5ENKnCaF0sE%3D",
          "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tiktok-obj/1625007705715714.webp?x-expires=1671868800&x-signature=u4vli7QIR8U0UtwS9qAGDVd1Iv0%3D",
          "https://p9-sign-sg.tiktokcdn.com/aweme/100x100/tiktok-obj/1625007705715714.webp?x-expires=1671868800&x-signature=67kmEym7VjqY68akAlZEJGrF5d4%3D",
          "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tiktok-obj/1625007705715714.jpeg?x-expires=1671868800&x-signature=4sLnk5Ulx3UwngRGI%2BbnbjVPVTM%3D",
        ],
      },
      followInfo: {
        followingCount: 4,
        followerCount: 3,
        followStatus: 0,
        pushStatus: 0,
      },
      isModerator: false,
      isNewGifter: false,
      isSubscriber: false,
      msgId: "7179880910576503578",
      createTime: "1671696363850",
    };

    this.event.reply("chat", testingData);
  }

  like() {
    const data = {
      likeCount: 1,
      totalLikeCount: 2,
      userId: "6556496534862004225",
      secUid:
        "MS4wLjABAAAAY4wPf3qn7uom6vSYU3EGFQ5eRdKW8w-5c-8DAp9mBWVZ4R-DWk1N8Jgao5tl5873",
      uniqueId: "crylmnlps",
      nickname: "Caryl Lorraine Tria Manlapas",
      profilePictureUrl:
        "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
      followRole: 1,
      userDetails: {
        createTime: "0",
        bioDescription: "",
        profilePictureUrls: [
          "https://p16-sign-va.tiktokcdn.com/tos-useast2a-avt-0068-giso/64ebbd76fde2a8b2895dea88e0eafe13~tplv-tiktok-shrink:72:72.webp?x-expires=1671872400&x-signature=g9keHef0JuLkbm8yrxpyBgy4p10%3D",
          "https://p16-sign-va.tiktokcdn.com/tos-useast2a-avt-0068-giso/64ebbd76fde2a8b2895dea88e0eafe13~c5_100x100.webp?x-expires=1671872400&x-signature=sGy692%2FsZjbDkMujCmhnx2mYhVY%3D",
          "https://p16-sign-va.tiktokcdn.com/tos-useast2a-avt-0068-giso/64ebbd76fde2a8b2895dea88e0eafe13~c5_100x100.jpeg?x-expires=1671872400&x-signature=nMOny7%2BDbnnO3Y8DdM7yyQbBr%2Fs%3D",
        ],
      },
      followInfo: {
        followingCount: 1465,
        followerCount: 681,
        followStatus: 1,
        pushStatus: 0,
      },
      isModerator: false,
      isNewGifter: false,
      isSubscriber: false,
      msgId: "7179908356915235611",
      createTime: "1671702685207",
      displayType: "pm_mt_msg_viewer",
      label: "{0:user} liked the LIVE",
    };
    this.event.reply("like", data);
  }

  share() {
    const data = {
      userId: "7106136438798664710",
      secUid:
        "MS4wLjABAAAAaXFtkK4KOl1LsS1SwyurE5DAIDeUt3AHZmAUACwaoHY3T3BG41FaUgFWEfiYtGa1",
      uniqueId: "anphatnguyen5",
      nickname: "anphatnguyen5",
      profilePictureUrl:
        "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146.webp?x-expires=1671872400&x-signature=8ejo5l%2F2rZuyXPQ7VJWG1DM8yxE%3D",
      followRole: 1,
      userDetails: {
        createTime: "0",
        bioDescription: "",
        profilePictureUrls: [
          "https://p16-sign-sg.tiktokcdn.com/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146~tplv-tiktok-shrink:72:72.webp?x-expires=1671872400&x-signature=eAD4uHgLJD2qxeEkulYDOBxjPSk%3D",
          "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146.webp?x-expires=1671872400&x-signature=8ejo5l%2F2rZuyXPQ7VJWG1DM8yxE%3D",
          "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146.jpeg?x-expires=1671872400&x-signature=QyTeKryteL%2BCuIp0dMBjw47L8Vc%3D",
        ],
      },
      followInfo: {
        followingCount: 44,
        followerCount: 4,
        followStatus: 1,
        pushStatus: 0,
      },
      isModerator: false,
      isNewGifter: false,
      isSubscriber: false,
      msgId: "7179909908505152257",
      createTime: "1671703045823",
      displayType: "pm_mt_guidance_share",
      label: "{0:user} shared the LIVE",
    };
    this.event.reply("share", data);
  }

  follow() {
    const data = {
      userId: "7106136438798664710",
      secUid:
        "MS4wLjABAAAAaXFtkK4KOl1LsS1SwyurE5DAIDeUt3AHZmAUACwaoHY3T3BG41FaUgFWEfiYtGa1",
      uniqueId: "anphatnguyen5",
      nickname: "anphatnguyen5",
      profilePictureUrl:
        "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146.webp?x-expires=1671872400&x-signature=8ejo5l%2F2rZuyXPQ7VJWG1DM8yxE%3D",
      followRole: 1,
      userDetails: {
        createTime: "0",
        bioDescription: "",
        profilePictureUrls: [
          "https://p16-sign-sg.tiktokcdn.com/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146~tplv-tiktok-shrink:72:72.webp?x-expires=1671872400&x-signature=eAD4uHgLJD2qxeEkulYDOBxjPSk%3D",
          "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146.webp?x-expires=1671872400&x-signature=8ejo5l%2F2rZuyXPQ7VJWG1DM8yxE%3D",
          "https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/13b7a35f3a1b4650e1b163a965270146.jpeg?x-expires=1671872400&x-signature=QyTeKryteL%2BCuIp0dMBjw47L8Vc%3D",
        ],
      },
      followInfo: {
        followingCount: 44,
        followerCount: 4,
        followStatus: 1,
        pushStatus: 0,
      },
      isModerator: false,
      isNewGifter: false,
      isSubscriber: false,
      msgId: "7179909565495233281",
      createTime: "1671702966031",
      displayType: "pm_main_follow_message_viewer_2",
      label: "{0:user} followed the host",
    };
    this.event.reply("follow", data);
  }

  gift() {
    const data = {
      giftId: 5655,
      groupId: "1671703122630",
      repeatCount: 133,
      userId: "109707597481410560",
      secUid:
        "MS4wLjABAAAAZh1QYbL055uZ8Ur8buWq9bQnCRv7JcyuCSgNFFx7ON0CvSnLa7Gw6JQ1hVIjwG3T",
      uniqueId: "lions1ngh",
      nickname: "Randeep Deol",
      profilePictureUrl:
        "https://p16-sign.tiktokcdn-us.com/tos-useast5-avt-0068-tx/8fe9b9ee7f0ff6626895dd9a84a87468~c5_100x100.webp?x-expires=1671872400&x-signature=IW7SJvFB98DDUVVn%2Fg1vf8uRm1k%3D",
      followRole: 0,
      userDetails: {
        createTime: "0",
        bioDescription: "",
        profilePictureUrls: [
          "https://p16-sign.tiktokcdn-us.com/tos-useast5-avt-0068-tx/8fe9b9ee7f0ff6626895dd9a84a87468~tplv-tiktok-shrink:72:72.webp?x-expires=1671872400&x-signature=g2q3iNRK2DprY5J1JkQEC1O61pc%3D",
          "https://p16-sign.tiktokcdn-us.com/tos-useast5-avt-0068-tx/8fe9b9ee7f0ff6626895dd9a84a87468~c5_100x100.webp?x-expires=1671872400&x-signature=IW7SJvFB98DDUVVn%2Fg1vf8uRm1k%3D",
          "https://p19-sign.tiktokcdn-us.com/tos-useast5-avt-0068-tx/8fe9b9ee7f0ff6626895dd9a84a87468~c5_100x100.webp?x-expires=1671872400&x-signature=KoENiVTV1VEvcFqjGY7MMVdm%2BtE%3D",
          "https://p16-sign.tiktokcdn-us.com/tos-useast5-avt-0068-tx/8fe9b9ee7f0ff6626895dd9a84a87468~c5_100x100.jpeg?x-expires=1671872400&x-signature=%2F%2FchLTCfKNa3bcpzYviGNhVegFA%3D",
        ],
      },
      followInfo: {
        followingCount: 6555,
        followerCount: 3456,
        followStatus: 0,
        pushStatus: 0,
      },
      isModerator: false,
      isNewGifter: false,
      isSubscriber: false,
      createTime: "1671703147173",
      msgId: "7179907833360861995",
      displayType: "webcast_aweme_gift_send_message",
      label: "{0:user} sent {1:gift} {2:string}",
      repeatEnd: false,
      gift: { gift_id: 5655, repeat_count: 133, repeat_end: 0, gift_type: 1 },
      giftName: "Rose",
      giftType: 1,
      diamondCount: 1,
      describe: "Sent Rose",
      giftPictureUrl:
        "https://p19-webcast.tiktokcdn.com/img/maliva/webcast-va/eba3a9bb85c33e017f3648eaf88d7189~tplv-obj.png",
      timestamp: 1671703147173,
      receiverUserId: "6828379625065989122",
      extendedGiftInfo: {
        action_type: 0,
        app_id: 0,
        business_text: "",
        can_put_in_gift_box: false,
        combo: true,
        deprecated10: false,
        deprecated11: false,
        deprecated12: 0,
        deprecated14: "",
        deprecated2: false,
        deprecated3: false,
        deprecated4: 0,
        deprecated6: 0,
        deprecated7: 0,
        deprecated8: 0,
        deprecated9: false,
        describe: "sent Rose",
        diamond_count: 1,
        duration: 1000,
        event_name: "livesdk_gift_click",
        for_custom: false,
        for_linkmic: true,
        gift_rank_recommend_info: "",
        gift_scene: 1,
        gold_effect: "",
        gray_scheme_url: "",
        guide_url: "",
        icon: {
          avg_color: "#DCF4FA",
          height: 0,
          image_type: 0,
          is_animated: false,
          open_web_url: "",
          uri: "webcast-va/eba3a9bb85c33e017f3648eaf88d7189",
          width: 0,
        },
        id: 5655,
        image: {
          avg_color: "#EBCEE1",
          height: 0,
          image_type: 0,
          is_animated: false,
          open_web_url: "",
          uri: "webcast-va/eba3a9bb85c33e017f3648eaf88d7189",
          width: 0,
        },
        is_box_gift: false,
        is_broadcast_gift: false,
        is_displayed_on_panel: true,
        is_effect_befview: false,
        is_gray: false,
        is_random_gift: false,
        item_type: 1,
        lock_info: { gift_level: 0, lock: false, lock_type: 0 },
        manual: "",
        name: "Rose",
        notify: false,
        primary_effect_id: 0,
        region: "",
        scheme_url: "",
        special_effects: {},
        tracker_params: {},
        type: 1,
      },
    };
    this.event.reply("gift", data);
  }
}
