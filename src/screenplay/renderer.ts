/* eslint-disable @typescript-eslint/ban-ts-comment */
import { app, contextBridge, ipcRenderer } from "electron";
import jQuery from "jquery";
require("./animate.css");
require("./main.css");
import introLogo from "./images/vs.png";
import gameOverImage from "./images/game-over.png";
import soundPlayingGame from "./sound/dota-main-sound.mp3";
import soundIntro from "./sound/dota-intro.mp3";
import soundGameOver from "./sound/game-over.mp3";
import _soundFXDrop from "./sound/drop.mp3";
import _soundFXBoom from "./sound/boom.mp3";
import _soundFXPop from "./sound/pop.mp3";

const soundIntroScreen = new Audio(soundIntro);
const soundPlayingScreen = new Audio(soundPlayingGame);
const soundGameOverScreen = new Audio(soundGameOver);
const soundFXBoom = new Audio(_soundFXBoom);
const soundFXPop = new Audio(_soundFXPop);
const soundFXDrop = new Audio(_soundFXDrop);

const bossRed = document.getElementById("boss_red");
const bossBlue = document.getElementById("boss_blue");
const bossRedDie = document.getElementById("boss_red_die");
const bossBlueDie = document.getElementById("boss_blue_die");

/**!SECTION
 * MaxListenersExceededWarning: Possible EventEmitter memory leak detected. 11 subwindow_connect listeners added to [IpcMainImpl]. Use emitter.setMaxListeners() to increase limit
 */

// https://stackoverflow.com/questions/49643248/html5-canvas-draw-lines-in-a-circle

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
Array.prototype.removeByValue = function (val) {
  for (let i = 0; i < this.length; i++) {
    if (this[i] === val) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

function randomRange(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function makeid(length = 8) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

/**
 * create circle
 * @param {*} fromID
 * @param {*} bossBlue
 * @param {*} extraClass
 */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore

// Fireee

function parabola(
  fromID: string,
  bossBlue: string,
  extraClass: string,
  callbackFunction: { (): void; (): void }
) {
  const self = document.querySelector(
    '[id="' + fromID + '"]'
  ) as HTMLElement | null;
  const targetDiv = document.querySelector(
    '[id="' + bossBlue + '"]'
  ) as HTMLElement | null;
  const circle = document.createElement("div");
  const randomName = "circle_" + makeid(12);
  circle.className = "circle " + extraClass + " " + randomName;
  let a = 0,
    b = 0,
    c = 0,
    x1 = 0,
    y1 = 0,
    x2 = 0,
    y2 = 0,
    y = 0,
    sum = 0;

  x1 = self.offsetLeft + self.clientWidth / 2;
  y1 = self.offsetTop + self.clientHeight / 2;
  x2 = targetDiv.offsetLeft + self.clientWidth / 2;
  y2 = targetDiv.offsetTop + self.clientHeight / 2;
  a = 0.01;
  b = (y1 - y2 - a * (x1 * x1 - x2 * x2)) / (x1 - x2);
  c = y1 - a * x1 * x1 - b * x1;
  sum = x1;
  y = a * sum * sum + b * sum + c;

  const cssInline = `
  .${randomName}{
    position: absolute;
    top: ${y}px;
    left: ${sum}px;
    animation: animate_${randomName} 2s linear 1;
    transform-origin: top;
  }

  @keyframes  animate_${randomName} {
    0% {
      transform: rotate(90deg) translate(0, 0) rotate(-90deg);
    }
    100% {
      transform: rotate(0deg) translate(${x2 - x1}px, ${y2 - y1}px) rotate(0);
    }  
  }
`;

  const createdStyleTag = document.createElement("style");
  createdStyleTag.textContent = cssInline;
  document.body.appendChild(createdStyleTag);
  document.body.appendChild(circle);
  setTimeout(function () {
    document.body.removeChild(circle);
    document.body.removeChild(createdStyleTag);
    callbackFunction();
  }, 2000);
}

function parabola_righttoleft(
  fromID: string,
  bossRed: string,
  extraClass: string,
  callbackFunction: { (): void; (): void }
) {
  const self = document.querySelector(
    '[id="' + fromID + '"]'
  ) as HTMLElement | null;
  const targetDiv = document.querySelector(
    '[id="' + bossRed + '"]'
  ) as HTMLElement | null;
  const circle = document.createElement("div");
  const randomName = "circle_" + makeid(12);
  circle.className = "circle " + extraClass + " " + randomName;
  let a = 0,
    b = 0,
    c = 0,
    x1 = 0,
    y1 = 0,
    x2 = 0,
    y2 = 0,
    y = 0,
    sum = 0;
  x1 = self.offsetLeft + self.clientWidth / 2;
  y1 = self.offsetTop + self.clientHeight / 2;
  x2 = targetDiv.offsetLeft + self.clientWidth / 2;
  y2 = targetDiv.offsetTop + self.clientHeight / 2;
  a = 0.01;
  b = (y1 - y2 - a * (x1 * x1 - x2 * x2)) / (x1 - x2);
  c = y1 - a * x1 * x1 - b * x1;
  sum = x1;
  y = a * sum * sum + b * sum + c;

  const cssInline = `
  .${randomName}{
    position: absolute;
    top: ${y}px;
    left: ${sum}px;
    animation: animate_${randomName} 2s linear 1;
    transform-origin: top;
  }

  @keyframes  animate_${randomName} {
    0% {
      transform: rotate(90deg) translate(0, 0) rotate(-90deg);
    }
    100% {
      transform: rotate(0deg) translate(${x2 - x1}px, ${y2 - y1}px) rotate(0);
    }  
  }

`;

  const createdStyleTag = document.createElement("style");
  createdStyleTag.textContent = cssInline;
  document.body.appendChild(createdStyleTag);
  document.body.appendChild(circle);
  setTimeout(function () {
    document.body.removeChild(circle);
    document.body.removeChild(createdStyleTag);
    callbackFunction();
  }, 2000);
}

// const formatNumber = (num: number) => {
//   const n = String(num),
//     p = n.indexOf(".");
//   return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
//     p < 0 || i < p ? `${m},` : m
//   );
// };

jQuery(function ($: any) {
  /**
   * Movable windows, DO NOT REMOVE ....
   */
  const windowTopBar: any = document.createElement("div");
  windowTopBar.style.width = "100%";
  windowTopBar.style.height = "86px";
  windowTopBar.style.backgroundColor = "transparent";
  windowTopBar.style.position = "absolute";
  windowTopBar.style.top = windowTopBar.style.left = 0;
  windowTopBar.style.top = windowTopBar.style.zIndex = 999999;
  windowTopBar.style.webkitAppRegion = "drag";
  document.body.appendChild(windowTopBar);

  /**
   * Game setting ...
   */
  let timeToPlayUntilEndgameInSecond = 60 * 30;
  const introTimeInSecond = 50;
  const all_characters_id: any[] = []; /** Save all ID in here */
  const character_processed: any[] =
    []; /** every character that jump in game ... for visual only */
  const maxBlood = 100;
  let redBlood = 100; /** initial red blood! */
  let blueBlood = 100; /** initial red blood! */

  let characterRedSide = 0;
  let characterBlueSide = 0;

  const maxWeaponDisplay = 50;

  const allCharactersPositionLeft: { top: number; left: number }[] =
    []; /** get all position for  runing later */
  const allCharactersPositionRight: { top: number; left: number }[] =
    []; /** get all position for  runing later */

  /**************************************************************** *
   * END CONFIG
  /**************************************************************** */

  /**
   * Check blood to endgame...
   */
  setInterval(() => {
    if (redBlood < 1 || blueBlood < 1) {
      resetBossHeal();
    }
  }, 1000);

  function resetBossHeal() {
    const reborn_cooldown_red = document.getElementById("reborn_cooldown_red");
    const reborn_cooldown_blue = document.getElementById(
      "reborn_cooldown_blue"
    );

    let rebornTimeRedBoss = 10;
    let rebornTimeBlueBoss = 10;
    let seconds = 0;

    const cooldownRebornRedBoss = setInterval(() => {
      rebornTimeRedBoss = rebornTimeRedBoss - 1;
      seconds = Math.floor(rebornTimeRedBoss % 60);

      // @ts-nocheck
      const secondsStringRedBoss: string =
        seconds < 10 ? `0${seconds}` : `${seconds}`;
      reborn_cooldown_red.innerHTML = secondsStringRedBoss;

      if (rebornTimeRedBoss < 0) {
        clearInterval(cooldownRebornRedBoss);
        redBlood = maxBlood;
        bossRed.style.display = "block";
        bossRedDie.style.display = "none";
      }
    }, 1000);

    const cooldownRebornBlueBoss = setInterval(() => {
      rebornTimeBlueBoss = rebornTimeBlueBoss - 1;
      seconds = Math.floor(rebornTimeBlueBoss % 60);

      // @ts-nocheck
      const secondsStringBlueBoss: string =
        seconds < 10 ? `0${seconds}` : `${seconds}`;
      reborn_cooldown_blue.innerHTML = secondsStringBlueBoss;

      if (rebornTimeBlueBoss < 0) {
        blueBlood = maxBlood;
        clearInterval(cooldownRebornBlueBoss);
        bossBlue.style.display = "block";
        bossBlueDie.style.display = "none";
      }
    }, 1000);

    if (redBlood < 1) {
      bossRed.style.display = "none";
      bossRedDie.style.display = "block";
    } else {
      bossRed.style.display = "block";
      bossRedDie.style.display = "none";
    }
    if (blueBlood < 1) {
      bossBlue.style.display = "none";
      bossBlueDie.style.display = "block";
    } else {
      bossBlue.style.display = "block";
      bossBlueDie.style.display = "none";
    }
  }

  const checkFinish = setInterval(() => {
    if (bossBlueRun >= -90 || bossRedRun >= -90) {
      clearInterval(checkFinish);
      return endgame();
    }
  }, 1000);

  let leftOrRight = 0;
  const firingInterval = setInterval(async () => {
    leftOrRight++;
    if (leftOrRight % 2 === 0) {
      // left firing
      const targetToBoom = bossBlue;
      if (!targetToBoom) return;
      const targetID = targetToBoom.id;
      const targetDIV: any = $('div[id="' + bossBlue + '"]');
      targetDIV.addClass("hurt shaking");
      setTimeout(function () {
        targetDIV.removeClass("hurt shaking");
      }, 5000);
      let maxDotedDisplayed = 0;
      soundFXPop.play();

      $("#characters_wrap_red .character_join").each(
        async (index: any, element: any) => {
          if ($(element).hasClass("jump") === false) return;
          if (maxDotedDisplayed > maxWeaponDisplay) return;
          const _id = $(element).prop("id");
          parabola(_id, targetID, "redside", () => {
            soundFXBoom.play();
          }); // 4CAF50
          maxDotedDisplayed++;
        }
      );

      setTimeout(() => {
        blueBlood = blueBlood - Number(characterRedSide);
      }, 2000);
    } else {
      // right firing
      const targetToBoom = bossRed;
      if (!targetToBoom) return;
      const targetID = targetToBoom.id;
      const targetDIV: any = $('div[id="' + bossRed + '"]');
      targetDIV.addClass("hurt shaking");
      setTimeout(function () {
        targetDIV.removeClass("hurt shaking");
      }, 5000);
      let maxDotedDisplayed = 0;
      soundFXPop.play();
      $("#characters_wrap_blue .character_join").each(
        (index: any, element: any): any => {
          if ($(element).hasClass("jump") === false) return;
          if (maxDotedDisplayed > maxWeaponDisplay) return;
          const _id = $(element).prop("id");
          parabola_righttoleft(_id, targetID, "blueside", () => {
            soundFXBoom.play();
          }); // 4CAF50
          maxDotedDisplayed++;
        }
      );

      setTimeout(() => {
        redBlood = redBlood - Number(characterBlueSide);
      }, 2000);
    }
  }, 5000);

  /**!SECTION
   * Animate character skydriving down!
   */
  const characterSkydriving = setInterval(() => {
    const character_join = $(".character_join");
    character_join.each((_index: any, element: any): any => {
      const divToProcess: any = $(element);
      const xDataID = divToProcess.data("id");

      if (character_processed.indexOf(xDataID) > -1) return;
      character_processed.push(xDataID);

      function randomTopPosition() {
        return randomRange(700, 700);
      }

      let default_landing: any = {
        top: randomTopPosition(),
        left: randomRange(5, 168),
      };

      if (divToProcess.hasClass("blue_side")) {
        default_landing = {
          top: randomTopPosition(),
          left: randomRange(286, 485),
        };
        const newx = default_landing;
        newx.id = xDataID;
        allCharactersPositionRight.push(newx);
      } else {
        const newx = default_landing;
        newx.id = xDataID;
        allCharactersPositionLeft.push(newx);
      }

      divToProcess.animate(default_landing, {
        duration: 2000,
        specialEasing: {
          width: "linear",
          height: "easeOutBounce",
        },
        complete: function () {
          divToProcess.removeClass("swing");
          divToProcess.addClass("flash2times");
          soundFXDrop.play();
          setTimeout(() => {
            divToProcess.addClass("no-background");
            divToProcess.addClass("jump");
          }, 1000);
        },
      });
    });
  }, 1500);

  function intro() {
    const intro_content = document.getElementById("intro_content");
    const intro_content_image = document.getElementById(
      "intro_content_image"
    ) as HTMLImageElement | null;
    intro_content_image.src = introLogo;
    soundIntroScreen.play();
    intro_content.style.display = "block";

    const countdownspan = document.getElementById("count_down_span");
    let iii = introTimeInSecond;
    const intervalCount = setInterval(() => {
      if (!countdownspan) return;
      iii--;
      countdownspan.innerText = String(iii);
      if (Number(iii) === 0) {
        clearInterval(intervalCount);
        soundIntroScreen.pause();
        intro_content.style.display = "none";
        intro_content.innerHTML = "";
      }
    }, 1000);

    setTimeout(() => {
      startGaming();
    }, introTimeInSecond * 1000);
  }
  intro();

  /**
   * Game over ...
   */
  function endgame() {
    clearInterval(characterSkydriving);
    clearInterval(firingInterval);
    const main_content = document.getElementById("main_content");
    const game_over = document.getElementById("game_over");
    const game_over_result = document.getElementById("game_over_result");
    const game_over_image = document.getElementById(
      "game_over_image"
    ) as HTMLImageElement | null;
    game_over_image.src = gameOverImage;
    // main_content.innerHTML = '';
    main_content.style.display = "none";
    game_over.style.display = "block";

    setTimeout(function () {
      main_content.innerHTML = "";
    }, 2000);

    /**!SECTION
     * Play sound
     */
    soundGameOverScreen.play();
    soundPlayingScreen.pause();

    /**
     * Who  win the game?
     */
    const nameofWinner = bossRedRun > bossBlueRun ? "RED team" : "BLUE team";
    game_over_result.innerText = nameofWinner;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    document.getElementById("total_player").innerText =
      all_characters_id.length;
    // @ts-ignore
    document.getElementById("total_player_redside").innerText =
      characterRedSide;
    // @ts-ignore
    document.getElementById("total_player_blueside").innerText =
      characterBlueSide;
  }

  var bossBlueRun = -500;
  var bossRedRun = -500;
  function startGaming() {
    const main_content = document.getElementById("main_content");
    main_content.style.display = "block";
    soundPlayingScreen.play(); // debug

    let minutes = 0,
      seconds = 0;
    const game_deadline_minute_count = document.getElementById(
      "game_deadline_minute_count"
    );
    const game_deadline_second_count = document.getElementById(
      "game_deadline_second_count"
    );

    /**
     * Caculate blood
     */
    const caculateBlood = setInterval(() => {
      timeToPlayUntilEndgameInSecond = timeToPlayUntilEndgameInSecond - 1;

      // left blood
      const w = Math.floor((redBlood / maxBlood) * 100);
      document.getElementById("span_heal_red").style.width = `${w}%`;

      // right blood
      const wf = Math.floor((blueBlood / maxBlood) * 100);
      document.getElementById("span_heal_blue").style.width = `${wf}%`;

      /**
       * Count
       */
      minutes = Math.floor(timeToPlayUntilEndgameInSecond / 60);
      seconds = Math.floor(timeToPlayUntilEndgameInSecond % 60);
      // @ts-nocheck
      const minutesString: string = minutes < 10 ? `0${minutes}` : `${minutes}`;
      // @ts-nocheck
      const secondsString: string = seconds < 10 ? `0${seconds}` : `${seconds}`;

      game_deadline_minute_count.innerHTML = minutesString;
      game_deadline_second_count.innerHTML = secondsString;

      if (timeToPlayUntilEndgameInSecond < 0) {
        clearInterval(caculateBlood);
        endgame();
      }
    }, 1000);
  }

  ipcRenderer.send("subwindow_connect", true);

  ipcRenderer.on("chat", (e, data) => {
    const commentContent = data.comment;

    // all_characters_id
    const character_avatar = data.profilePictureUrl;
    const character_ID = data.userId;

    if (all_characters_id.indexOf(character_ID) > -1) return;
    all_characters_id.push(character_ID);

    let character_html = "";

    switch (commentContent) {
      case "123":
        // red side
        characterRedSide++;
        character_html = `
        <div class="character_join red_side swing" data-id="${character_ID}" id="${character_ID}">
          <img src="${character_avatar}" alt="" class="character_avatar" />
        </div>
        `;
        $("#characters_wrap_red").append(character_html);
        break;
      case "456":
        // blue side
        characterBlueSide++;
        character_html = `
        <div class="character_join blue_side swing" data-id="${character_ID}" id="${character_ID}">
          <img src="${character_avatar}" alt="" class="character_avatar" />
        </div>
        `;
        $("#characters_wrap_blue").append(character_html);
        break;
    }
  });

  let amifree = true;
  ipcRenderer.on("share", (e, data) => {
    if (blueBlood >= 1) {
      bossBlueRun = bossBlueRun + 5;
      bossBlue.style.bottom = `${bossBlueRun}px`;
      bossBlueDie.style.bottom = `${bossBlueRun}px`;
    }
  });

  ipcRenderer.on("like", (e, data) => {
    if (redBlood >= 1) {
      bossRedRun = bossRedRun + 5;
      bossRed.style.bottom = `${bossRedRun}px`;
      bossRedDie.style.bottom = `${bossRedRun}px`;
    }
  });

  ipcRenderer.on("gift", (e, data) => {
    if (amifree === false) return;
    amifree = false;
    const giftyWrap = document.getElementById("gifty");
    const whogift_avatar = document.getElementById(
      "whogift_avatar"
    ) as HTMLImageElement | null;
    whogift_avatar.src = data.profilePictureUrl;
    const whogift_name = document.getElementById("whogift_name");
    whogift_name.innerText = data.nickname;

    giftyWrap.style.display = "block";
    setTimeout(() => {
      giftyWrap.style.display = "none";
      amifree = true;
    }, 8000);
  });

  ipcRenderer.on("follow", (e, data) => {
    if (amifree === false) return;
    amifree = false;
    const giftyWrap = document.getElementById("gifty");
    const whogift_avatar = document.getElementById(
      "whogift_avatar"
    ) as HTMLImageElement | null;
    whogift_avatar.src = data.profilePictureUrl;
    const whogift_name = document.getElementById("whogift_name");
    whogift_name.innerText = `Welcome new Follower: ${data.nickname}`;

    giftyWrap.style.display = "block";
    setTimeout(() => {
      giftyWrap.style.display = "none";
      amifree = true;
    }, 8000);
  });
});
